#!/usr/bin/env bash
{% if dehydrated__dns_challenge %}
/etc/dehydrated/hooks.d/acmedns-hook.sh "$@"
{% endif %}

deploy_cert() {
    local DOMAIN="${1}" KEYFILE="${2}" CERTFILE="${3}" FULLCHAINFILE="${4}" CHAINFILE="${5}" TIMESTAMP="${6}"

    find /etc/dehydrated/hooks.d -maxdepth 1 -executable -name deploy-cert-\* -type f -exec sudo {} "$@" \;
}

deploy_ocsp() {
    local DOMAIN="${1}" OCSPFILE="${2}" TIMESTAMP="${3}"

    find /etc/dehydrated/hooks.d -maxdepth 1 -executable -name deploy-ocsp-\* -type f -exec sudo {} "$@" \;
}

HANDLER="$1"; shift
if [[ "${HANDLER}" =~ ^(deploy_cert|deploy_ocsp)$ ]]; then
  "$HANDLER" "$@"
fi
