#!/bin/bash
# {{ ansible_managed }}

set -e

CRED=/etc/dehydrated/acmedns-creds.json
ACC=/etc/dehydrated/config.acmedns

install -m 600 /dev/null "$CRED"
install -m 640 -g '{{ dehydrated__group }}' /dev/null "$ACC"

curl -s -d '{"allowfrom": ["{{ansible_default_ipv4.address}}/32"{% if ansible_default_ipv6.address is defined %},"{{ansible_default_ipv6.address}}/128"{% endif %}]}' \
	-X POST https://{{ dehydrated__acmedns_server }}/register > $CRED

echo "ACMEDNS_USERNAME[\"{{ dehydrated__domain[0].name }}\"]=$(jq ".username" < $CRED)" >> $ACC
echo "ACMEDNS_PASSWORD[\"{{ dehydrated__domain[0].name }}\"]=$(jq ".password" < $CRED)" >> $ACC
echo "ACMEDNS_SUBDOMAIN[\"{{ dehydrated__domain[0].name }}\"]=$(jq ".subdomain" < $CRED)" >> $ACC

exit 0
